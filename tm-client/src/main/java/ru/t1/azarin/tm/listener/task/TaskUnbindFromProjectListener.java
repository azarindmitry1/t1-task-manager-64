package ru.t1.azarin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectListener extends AbstractTaskListener {

    @NotNull
    public final static String NAME = "task-unbind-from-project";

    @NotNull
    public final static String DESCRIPTION = "Unbind task from project.";

    @Override
    @EventListener(condition = "@taskUnbindFromProjectListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpoint.unbindTaskFromProjectResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
