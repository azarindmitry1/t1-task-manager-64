package ru.t1.azarin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.event.ConsoleEvent;

import java.sql.SQLException;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    public final static String NAME = "user-logout";

    @NotNull
    public final static String DESCRIPTION = "Logout current user.";

    @Override
    @EventListener(condition = "@userLogoutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        authEndpoint.logout(request);
        setToken(null);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
