package ru.t1.dazarin.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.dazarin.tm")
public class ApplicationConfiguration {

}
