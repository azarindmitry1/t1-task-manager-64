package ru.t1.dazarin.tm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.dazarin.tm.enumerated.Status;
import ru.t1.dazarin.tm.model.Project;
import ru.t1.dazarin.tm.repository.ProjectRepository;

@Controller
@RequiredArgsConstructor
public final class ProjectController {

    private final ProjectRepository projectRepository;

    @PostMapping(value = "/project/create")
    public String create() {
        projectRepository.save(new Project("New Project " + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @GetMapping(value = "/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @GetMapping(value = "/project/update/{id}")
    public ModelAndView update(@PathVariable("id") String id) {
        final Project project = projectRepository.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-update");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @PostMapping(value = "/project/update/{id}")
    public String update(@ModelAttribute("project") Project project, BindingResult result) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @GetMapping(value = "/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectRepository.findAll());
    }

}
