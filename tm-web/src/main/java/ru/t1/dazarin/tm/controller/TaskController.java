package ru.t1.dazarin.tm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.dazarin.tm.enumerated.Status;
import ru.t1.dazarin.tm.model.Task;
import ru.t1.dazarin.tm.repository.ProjectRepository;
import ru.t1.dazarin.tm.repository.TaskRepository;

@Controller
@RequiredArgsConstructor
public final class TaskController {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    @PostMapping("/task/create")
    public String create() {
        taskRepository.save(new Task("New Task" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/update/{id}")
    public ModelAndView update(@PathVariable("id") String id) {
        final Task task = taskRepository.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-update");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projectRepository.findAll());
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @PostMapping("/task/update/{id}")
    public String update(@ModelAttribute("task") Task task) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/tasks")
    public ModelAndView index() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("projectRepository", projectRepository);
        modelAndView.addObject("tasks", taskRepository.findAll());
        return modelAndView;
    }

}
